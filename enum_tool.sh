#----Created by SPC Bustillo, Miguel and SSG Peterson, Thomas----
#----Enum Tool is used to enumerate the network for the SAD Missions----
#----Directory requires: Scoper.sh ~/Tools/scoper and Gnamp-Parser.sh ~/Tools/gnmap-parser
#----Tools: Scoper.sh, nmap, Gnmap-Parser.sh----
clear
echo '================================================================'
echo ' enum_tool.sh | [Version]: 1.4.0 | [Updated]: 07.28.2017'
echo '     ______                         ______            __ '
echo '    / ____/___  __  ______ ___     /_  __/___  ____  / / '
echo '   / __/ / __ \/ / / / __ `__ \     / / / __ \/ __ \/ /  '
echo '  / /___/ / / / /_/ / / / / / /    / / / /_/ / /_/ / /   '
echo ' /_____/_/ /_/\__,_/_/ /_/ /_/    /_/  \____/\____/_/    '
echo '                                                         '
echo '================================================================'


#----Variables 

ScopDir=~/Tools/scoper
GnmapDir=~/Tools/gnmap-parser
SadDir=~/SAD_Missions

#----MIS is Mission number----

read -r -p 'What Mission is this? ' MIS

#----MISDIR is Mission Directory----

cd ~
MISDIR=Mission_$MIS

#----Checks if the "SAD Missions" Directory exists----

if [ ! -d "$SadDir" ]; 
  	then
	  	mkdir $SadDir 
fi
cd $SadDir
#----Checks if the Mission # Directory exists----

if [ ! -d "$MISDIR" ]; 
  	then
	  	mkdir $MISDIR
		echo 'Please edit Include.txt, this is the IP range included in the NMAP scan.'	
		gedit $ScopDir/include.txt
		echo 'Please edit Exclude.txt, this is the IP range excluded in the NMAP scan.'
		gedit $ScopDir/exclude.txt
	else
		read -p "This Mission already exists, you will be overwriting it.
Would you like to continue? Y/N: " choice
		while true
		do
			case $choice in
				[yY])
			rm -r $SadDir/$MISDIR/Parsed-Results
			break 
			;;
				[nN])
			echo "Exiting... " && exit 1
			;;
				*)
			read -p "Invalid choice. Would you like to continue? Y/N: " choice
			;;
		esac
	done
fi

#----Exists if scoper is missing----

if [ -d "$ScopDir" ]; 
	then
		cd $ScopDir
	else
		echo "Scoper.sh is missing... exiting" && exit 1
fi

#----Exists if gnmap parser is missing---- 

if [ ! -d "$GnmapDir" ]; 
	then
		echo " Gnmap-Parser.sh is missing... exiting" && exit 1
fi


#----NUM is # of partitions to cut the IP Range into----

read -r -p 'How many partitions do you want? ' NUM

./Scoper.sh -i include.txt -e exclude.txt -s $NUM -p $MISDIR -o inscope.txt

cd $ScopDir
for ((x=0; x<$NUM; x++))
do
	mv $MISDIR-0$x $SadDir/$MISDIR
done
mv inscope.txt $SadDir/$MISDIR

#----partscan is the Partition to be scanned----

read -r -p 'Which partition do you want to scan? ' partscan
x=$(expr $partscan - 1)
filescan=$MISDIR-0$x
cd $SadDir/$MISDIR

#----nmap scan scans the IP addresses in the Partition----

nmap -Pn -n -sS -p 21-23,25,53,111,137,139,445,80,443,8443,8080,1433,3306,3389 --min-hostgroup 255 --min-rtt-timeout 0ms --max-rtt-timeout 250ms --max-retries 0 --max-scan-delay 0 -oA $filescan -vvv --open -iL $filescan

#----gnmap parses the data produced and finds active hosts for then NESSUS scan----
mv $filescan.gnmap $GnmapDir
cd $GnmapDir
./Gnmap-Parser.sh -p
mv -f $GnmapDir/$filescan.gnmap  $SadDir/$MISDIR
mv -f $GnmapDir/Parsed-Results $SadDir/$MISDIR
